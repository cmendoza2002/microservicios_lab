package com.microservicios.propietarios.dominio.repositorios;

import com.microservicios.propietarios.dominio.Propietario;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PropietariosRepositorio extends JpaRepository<Propietario,Long> {
}
