package com.microservicios.propietarios.controladores;

import com.microservicios.propietarios.dominio.Propietario;
import com.microservicios.propietarios.dominio.repositorios.PropietariosRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class PropietariosControlador {

    @Autowired
    PropietariosRepositorio repositorio;

    @GetMapping("/propietarios")
    public List<Propietario> getPropietarios()
    {
        return this.repositorio.findAll();
    }

    @GetMapping("/propietario/{id}")
    public Propietario consultarPorId(@PathVariable Long id)
    {
        Optional<Propietario> propietarioOptional = repositorio.findById(id);
        if (propietarioOptional.isPresent())
            return propietarioOptional.get();
        return null;
    }

    @PostMapping("/propietario")
    public Propietario crearPropietario(@RequestBody Propietario propietario)
    {
        Propietario entity =   repositorio.save(propietario);
        return entity;
    }

    @DeleteMapping("/propietario/{id}")
    public void eliminar(@PathVariable Long id)
    {
        Optional<Propietario> propietarioOptional   = repositorio.findById(id);
        if (propietarioOptional.isPresent())
            repositorio.delete(propietarioOptional.get());
    }


    @PutMapping("/propietario")
    public Propietario editarPropietario(@RequestBody Propietario especie)
    {
        Propietario entity =   repositorio.save(especie);
        return entity;
    }



}
