package com.microservicios.visitas.dominio;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Getter @Setter
@Entity
public class Visita {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String observaciones;
    private Date fecha;

    private Long mascotaId;
    private String nombreMascota;
}
