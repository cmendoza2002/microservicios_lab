package com.microservicios.mascotas.dto;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class EspecieDto {
    private Long id;
    private String nombre;
}
