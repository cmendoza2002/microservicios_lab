package com.microservicios.propietarios.config;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import springfox.documentation.RequestHandler;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Component
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket api()
    {
        Docket docket = new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.microservicios.propietarios.controladores"))
                .paths(PathSelectors.any())
                .build().apiInfo(getInfo());
        return docket;
    }

    public ApiInfo getInfo()
    {
        ApiInfo apiInfo = new ApiInfoBuilder()
                .title("Api Rest de propietarios")
                .description("Permite mantener el catalogo de propietarios")
                .contact(new Contact("Carlos Mendoza","hhtps://blog.com",""))
                .license("Apache 2.0")
                .licenseUrl("http://apache.org/licence")
                .version("v1.0")
                .build();
        return apiInfo;
    }

}
