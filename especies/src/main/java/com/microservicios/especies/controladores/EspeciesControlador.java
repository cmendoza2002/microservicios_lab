package com.microservicios.especies.controladores;

import com.microservicios.especies.dominio.Especie;
import com.microservicios.especies.dominio.repositorios.EspeciesRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class EspeciesControlador {

  @Autowired
  private EspeciesRepositorio especiesRepositorio;

  @GetMapping("/especies")
  public List<Especie> consultarEspecies()
  {
      List<Especie> lista = especiesRepositorio.findAll();
      return lista;
  }

  @GetMapping("/especie/{id}")
  public Especie consultarPorId(@PathVariable Long id)
  {
      System.out.println("Paso por aqui");
      Optional<Especie> especieOptional = especiesRepositorio.findById(id);
      if (especieOptional.isPresent())
          return especieOptional.get();
      return null;
  }

  @PostMapping("/especie")
  public Especie crearEspecie(@RequestBody Especie especie)
  {
     Especie entity =   especiesRepositorio.save(especie);
     return entity;
  }

  @DeleteMapping("/especie/{id}")
  public void eliminar(@PathVariable Long id)
  {
      Optional<Especie> especieOptional = especiesRepositorio.findById(id);
      if (especieOptional.isPresent())
            especiesRepositorio.delete(especieOptional.get());
  }


  @PutMapping("/especie")
  public Especie editarEspecie(@RequestBody Especie especie)
  {
        Especie entity =   especiesRepositorio.save(especie);
        return entity;
  }



}
