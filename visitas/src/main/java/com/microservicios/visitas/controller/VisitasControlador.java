package com.microservicios.visitas.controller;

import com.microservicios.visitas.dominio.Visita;
import com.microservicios.visitas.dominio.infraestructura.MascotasClienteApi;
import com.microservicios.visitas.dominio.repositorio.VisitaRepositorio;
import com.microservicios.visitas.dtos.MascotaDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class VisitasControlador {

    @Autowired
    VisitaRepositorio repositorio;

    @Autowired
    private MascotasClienteApi clienteApi;

    @PostMapping("/visita")
    public Visita crearVista(@RequestBody Visita visita)
    {
        MascotaDto mascotaDto = this.clienteApi.consultar(visita.getMascotaId());
        if(mascotaDto==null)
            return null;
        visita.setNombreMascota(mascotaDto.getNombre());

        Visita entity =   repositorio.save(visita);
        return entity;
    }


    @GetMapping("/visitas")
    public List<Visita> getVistas()
    {
        return this.repositorio.findAll();
    }

    @GetMapping("/visita/{id}")
    public Visita consultarPorId(@PathVariable Long id)
    {
        Optional<Visita> visitaOptional = repositorio.findById(id);
        if (visitaOptional.isPresent())
            return visitaOptional.get();
        return null;
    }



    @DeleteMapping("/visita/{id}")
    public void eliminar(@PathVariable Long id)
    {
        Optional<Visita> visitaOptional    = repositorio.findById(id);
        if (visitaOptional.isPresent())
            repositorio.delete(visitaOptional.get());
    }


    @PutMapping("/propietario")
    public Visita editarPropietario(@RequestBody Visita visita)
    {
        Visita entity =   repositorio.save(visita);
        return entity;
    }



}
