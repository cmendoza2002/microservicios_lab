package com.microservicios.mascotas.dto;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class PropietarioDto {
    private Long id;
    private String nombre;
}
