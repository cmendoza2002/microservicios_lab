package com.microservicios.oauth;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controlador {

    @GetMapping("/saludar")
    public String saludar(){
        return "Hola desde controlador estas auntenticado";
    }
}
