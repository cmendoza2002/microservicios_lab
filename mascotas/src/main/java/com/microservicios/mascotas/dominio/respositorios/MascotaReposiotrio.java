package com.microservicios.mascotas.dominio.respositorios;

import com.microservicios.mascotas.dominio.Mascota;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MascotaReposiotrio extends JpaRepository<Mascota,Long> {
}
