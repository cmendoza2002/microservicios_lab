package com.microservicios.especies.dominio.repositorios;

import com.microservicios.especies.dominio.Especie;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EspeciesRepositorio extends JpaRepository<Especie, Long> {
}
