package com.microservicios.visitas.dominio.infraestructura;

import com.microservicios.visitas.dtos.MascotaDto;
import com.microservicios.visitas.infraestructuraimpl.MascotasClienteApiLocal;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "MASCOTAS-API", fallback = MascotasClienteApiLocal.class)
public interface MascotasClienteApi {

    @CachePut("mascotas")
    @GetMapping("/mascota/{id}")
    MascotaDto consultar(@PathVariable Long id);
}
