package com.microservicios.visitas.infraestructuraimpl;

import com.microservicios.visitas.dominio.infraestructura.MascotasClienteApi;
import com.microservicios.visitas.dtos.MascotaDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Component;

@Component
public class MascotasClienteApiLocal implements MascotasClienteApi {

    @Autowired
    private CacheManager cacheManager;


    @Override
    public MascotaDto consultar(Long id) {

        Cache.ValueWrapper wrapper = cacheManager.getCache("mascotas").get(id);
        if(wrapper!=null)
        {
            return (MascotaDto)wrapper.get();
        }
        else {
            MascotaDto mascotaDto = new MascotaDto();
            mascotaDto.setId(0L);
            mascotaDto.setNombre("Desconocido");
            return mascotaDto;
        }
    }
}
