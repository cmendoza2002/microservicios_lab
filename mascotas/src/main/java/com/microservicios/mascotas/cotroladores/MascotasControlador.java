package com.microservicios.mascotas.cotroladores;

import com.microservicios.mascotas.dominio.Mascota;
import com.microservicios.mascotas.dominio.respositorios.MascotaReposiotrio;
import com.microservicios.mascotas.dto.EspecieDto;
import com.microservicios.mascotas.dto.PropietarioDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Optional;

@RestController
public class MascotasControlador {

    @Autowired
    private MascotaReposiotrio reposiotrio;

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("/macotas")
    public List<Mascota> consultarMascotas()
    {
        List<Mascota> lista = reposiotrio.findAll();
        return lista;
    }

    @GetMapping("/mascota/{id}")
    public Mascota consultarPorId(@PathVariable Long id)
    {
        Optional<Mascota> mascotaOptional = reposiotrio.findById(id);
        if (mascotaOptional.isPresent())
            return mascotaOptional.get();
        return null;
    }

    @PostMapping("/mascota")
    public ResponseEntity<Mascota>   crearMascota(@RequestBody Mascota mascota)
    {
        //Consulta especie por id
        //RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<EspecieDto> response = restTemplate
.getForEntity("http://ESPECIES-API/especie/" + mascota.getEspecieId(),
        EspecieDto.class);

        if(response.getBody()==null)
            return null;

        ResponseEntity<PropietarioDto> response2 = restTemplate
        .getForEntity("http://PROPIETARIOS-API/propietario/"+ mascota.getPropietarioId(),
                        PropietarioDto.class);

        if(response2.getBody()==null) {
            HttpHeaders headers = new HttpHeaders();
            headers.add("error","propietario no encontrado");
            ResponseEntity<Mascota> response3 =
                    new ResponseEntity<>(null, headers, HttpStatus.NOT_FOUND);

            return response3;
        }

        mascota.setEspecieId(response.getBody().getId());
        mascota.setNombreEspecie(response.getBody().getNombre());

        mascota.setNombrePropietario(response2.getBody().getNombre());
        mascota.setPropietarioId(response2.getBody().getId());

        Mascota entity = this.reposiotrio.save(mascota);
        return new ResponseEntity<>(mascota, HttpStatus.OK);
    }

    @DeleteMapping("/mascota/{id}")
    public void eliminar(@PathVariable Long id)
    {
        Optional<Mascota> mascotaOptional = reposiotrio.findById(id);
        if (mascotaOptional.isPresent())
            reposiotrio.delete(mascotaOptional.get());
    }


    @PutMapping("/mascota")
    public Mascota editarMascota(@RequestBody Mascota mascota)
    {
        Mascota entity =   reposiotrio.save(mascota);
        return entity;
    }


}
