package com.microservicios.visitas.dominio.repositorio;

import com.microservicios.visitas.dominio.Visita;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface VisitaRepositorio extends JpaRepository<Visita,Long> {

    //@Query("select v from Visita  v")
    //List<Visita> xyz();
}
